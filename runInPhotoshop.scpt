on run arg

  set fileName to arg's item 1
  set theFile to POSIX path of (fileName)

  open for access theFile
  set fileContents to (read theFile)
  close access theFile

  tell application "Adobe Photoshop CC"
    do javascript fileContents
    activate
  end tell

end run